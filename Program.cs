﻿using System;

namespace exercise_18
{
    class Program
    {
        static void Main(string[] args)
        {
        //Define 5 variables
            var num1 = 4;
            var num2 = 6;
            var num3 = 8;
            var num4 = 10;
            var num5 = 12;

            Console.WriteLine($"num1 = {num1}");
            Console.WriteLine($"num2 = {num2}");
            Console.WriteLine($"num3 = {num3}");
            Console.WriteLine($"num4 = {num4}");
            Console.WriteLine($"num5 = {num5}");
            //Display each variable and the name to the screen

            Console.WriteLine("");
            
            //Display 6 new variables
            var num6 = 1;
            var num7 = 3;
            var num8 = 5;
            var num9 = 7;
            var num10 = 9;
            var num11 = 11;

            //display sum of pairs of variables to the screen
            Console.WriteLine($"num6 + num7 = {num6+num7}");
            Console.WriteLine($"num8 + num9 = {num8+num9}");
            Console.WriteLine($"num10 + num11 = {num10+num11}");

            //display 4 new variables
            var num12 = 12;
            var num13 = 13;
            var num14 = 14;
            var num15 = 15;

            //display the sum of 2 variables and place the answer in the original variable value
            Console.WriteLine($"num12 = {num12+=num13}");
            Console.WriteLine($"num14 = {num14+=num15}");
            
            //declare string variable
            var think1 = "I am Thinking...";

            //Can not complie as a string var can not be multiplied by a int var
            Console.WriteLine($"think1 x 3 ={think1*3}");
        }
    }
}
